#!/usr/bin/env python3

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from pathlib import Path
from datetime import date
import os
import logging

root_dir = os.getcwd()
out_dir = root_dir + "/out"
name = "KarlHeinz"
footer= "generated with KarlHeinz"

try:
    os.mkdir(out_dir)
    logging.info("Created root output directory at: " + out_dir)
except FileExistsError:
    logging.info("Root output directory already exists!")

def generate_menu(output_file):
    output_file.write("<div class=\"menubar\">\n" + name + "\n")
    output_file.write("<a href=\"/index.html\">Home</a>\n")
    for x in Path(root_dir).iterdir():
        if x.is_dir():
            if str(x) == out_dir:
                continue
            output_file.write("<a href=\"/" + x.name + "\">" + x.stem.capitalize() + "</a>\n")
        else:
            if x.stem == "index":
                continue
            output_file.write("<a href=\"/" + x.stem + ".html\">" + x.stem.capitalize() + "</a>\n")
    output_file.write("</div>\n")

def write_footer(output_file):
    output_file.write("<div class=\"footer\">\n" + footer + "\n</div>\n")

def generate_index(path):
    f_out = open(str(out_dir) + "/" + str(path.relative_to(root_dir)) + "/index.html", "a")
    logging.info("Created index file: " + f_out.name)
    f_out.write("<html>\n<head>\n<title>" + root_dir.split('/')[-1] + "</title>\n</head>\n<body>\n")
    generate_menu(f_out)
    for x in Path(path).iterdir():
        if x.is_dir():
            continue
        date_created = date.fromtimestamp(os.stat(str(x)).st_ctime)
        for line in open(x).readlines():
            if line.startswith("# "):
                f_out.write("<a href=\"/" + str(x.relative_to(root_dir))[:-4] + ".html\">" + str(date_created) + " " + line.strip("#").strip() + "</a>")
                break
    f_out.write("</body>\n</html>")
    f_out.close()

def walker(path):
    for x in Path(path).iterdir():
        if x.is_dir():
            if str(x) == out_dir:
                continue
            try:
                os.mkdir(str(out_dir) + "/" + str(x.relative_to(root_dir)))
                logging.info("Created directory at: " + str(out_dir) + "/" + str(x.relative_to(root_dir)))
            except FileExistsError:
                logging.info("Output directory already exists!")
            if str(x) != out_dir:
                generate_index(x)
            walker(x)
        if x.suffix == ".gmi":
            writer(x)

def writer(gmi_file):
    f_out = open(str(out_dir) + "/" + str(gmi_file.relative_to(root_dir).parent) + "/" + gmi_file.stem + ".html", "a")
    logging.info("Created file: " + f_out.name)
    f_out.write("<html>\n<head>\n<title>" + root_dir.split('/')[-1] + "</title>\n</head>\n<body>\n")
    generate_menu(f_out)
    for line in open(gmi_file).readlines():
        if line.startswith("# "):
            f_out.write("<h1>" + line.strip("# ").strip() + "</h1>\n")
        elif line.startswith("## "):
            f_out.write("<h2>" + line.strip("## ").strip() + "</h2>\n")
        elif line.startswith("### "):
            f_out.write("<h3>" + line.strip("### ").strip() + "</h3>\n")
        elif line.startswith("=> "):
            line_parts = line.split(" ")
            f_out.write("<a href=\"" + line_parts[1] + "\">" + " ".join(line_parts[2:]).strip() + "</a>")
        elif line.endswith("\n"):
            f_out.write(line.strip() + "<br>\n")
        else:
            f_out.write(line)
    write_footer(f_out)
    f_out.write("</body>\n</html>")
    f_out.close()

walker(root_dir)
